import React, { Component } from "react";
import "./App.css";
import Header from "./components/Header";
import Menu from "./components/Menu";
import People from "./components/People";
import Planet from "./components/Planet";
import Film from "./components/Film";
import Specie from "./components/Specie";
import Vehicle from "./components/Vehicle";
import Starship from "./components/Starship";

import { BrowserRouter as Router, Route } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Header />
          <Route exact path="/" component={Menu} />
          <Route path="/people" component={People} />
          <Route path="/planets" component={Planet} />
          <Route path="/films" component={Film} />
          <Route path="/species" component={Specie} />
          <Route path="/vehicles" component={Vehicle} />
          <Route path="/starships" component={Starship} />
        </div>
      </Router>
    );
  }
}

export default App;
