import axios from "axios";

const CountRequest = async path => {
  const response = await axios.get(`http://swapi1111.herokuapp.com/${path}/`);
  return response.data.count;
};

const RandomIdRequest = async (path, count) => {
  const idNumber = 1 + Math.random() * (count - 1);
  const requestURI =
    `http://swapi1111.herokuapp.com/${path}/` + Math.floor(idNumber);
  const response = await axios.get(requestURI);

  return response.data;
};

const SpecificRequest = async path => {
  const response = await axios.get(path);

  return response.data;
};

export { RandomIdRequest, CountRequest, SpecificRequest };
