import React from "react";
import { CountRequest, RandomIdRequest, SpecificRequest } from "../apiRequest";
import PlanetInfo from "./PlanetInfo";
import NextPlanetButton from "./NextPlanetButton";

class Content extends React.Component {
  state = {
    isFetching: true,
    person: {},
    specie: String,
    homeworld: String,
    count: Number
  };

  componentWillMount() {
    CountRequest("people").then(response => {
      this.setState(
        {
          count: response
        },
        () => {
          this.personRequest();
        }
      );
    });
  }

  personRequest = () => {
    this.setState({
      isFetching: true
    });
    RandomIdRequest("people", this.state.count).then(response => {
      this.setState(
        {
          person: response
        },
        () => {
          SpecificRequest(this.state.person.species[0])
            .then(response => {
              this.setState(
                {
                  specie: response.name
                },
                () => {
                  SpecificRequest(this.state.person.homeworld).then(
                    response => {
                      this.setState({
                        homeworld: response.name,
                        isFetching: false
                      });
                    }
                  );
                }
              );
            })
            .catch(err => {
              this.setState(
                {
                  specie: "unknown"
                },
                () => {
                  SpecificRequest(this.state.person.homeworld).then(
                    response => {
                      this.setState({
                        homeworld: response.name,
                        isFetching: false
                      });
                    }
                  );
                }
              );
            });
        }
      );
    });
  };

  render() {
    return (
      <div className="content">
        <PlanetInfo
          title={this.state.person.name}
          firstItemKey={Object.keys(this.state.person)[0]}
          firstItemValue={this.state.specie}
          secondItemKey={Object.keys(this.state.person)[5]}
          secondItemValue={this.state.person.gender}
          thirdItemKey={Object.keys(this.state.person)[6]}
          thirdItemValue={this.state.homeworld}
          fourthItemKey={Object.keys(this.state.person)[1]}
          fourthItemValue={
            this.state.person.films && this.state.person.films.length
          }
          fetching={this.state.isFetching}
        />
        <NextPlanetButton
          clickHandler={this.personRequest}
          disabled={this.state.isFetching}
        />
      </div>
    );
  }
}

export default Content;
