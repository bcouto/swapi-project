import React from "react";

const ButtonNotDisabled = props => (
  <div className="next-planet-button">
    <button
      onClick={() => {
        props.clickHandler();
      }}
    >
      Next Planet
    </button>
  </div>
);

const ButtonDisabled = () => (
  <div className="next-planet-button">
    <button disabled>Next Planet</button>
  </div>
);

const NextPlanetButton = props => {
  if (props.disabled) {
    return ButtonDisabled();
  } else {
    return ButtonNotDisabled(props);
  }
};

export default NextPlanetButton;
