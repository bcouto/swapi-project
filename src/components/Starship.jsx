import React from "react";
import { CountRequest, RandomIdRequest } from "../apiRequest";
import PlanetInfo from "./PlanetInfo";
import NextPlanetButton from "./NextPlanetButton";

class Content extends React.Component {
  state = {
    isFetching: true,
    planet: {},
    count: Number
  };

  componentWillMount() {
    CountRequest("starships").then(response => {
      this.setState(
        {
          count: response
        },
        () => {
          this.loadPlanet();
        }
      );
    });
  }

  loadPlanet = () => {
    this.setState({
      isFetching: true
    });
    RandomIdRequest("starships", this.state.count).then(response => {
      this.setState({
        planet: response,
        isFetching: false
      });
    });
  };

  render() {
    return (
      <div className="content">
        <PlanetInfo
          planetName={this.state.planet.name}
          planetPopulation={this.state.planet.population}
          planetClimate={this.state.planet.climate}
          planetTerrain={this.state.planet.terrain}
          planetFilms={
            this.state.planet.films && this.state.planet.films.length
          }
          fetching={this.state.isFetching}
        />
        <NextPlanetButton
          clickHandler={this.loadPlanet}
          disabled={this.state.isFetching}
        />
      </div>
    );
  }
}

export default Content;
