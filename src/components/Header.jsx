import React from "react";
import logo from "../assets/images/logo.png";

const Header = function() {
  return (
    <div className="Header">
      <img className="logoImage" src={logo} alt="logo" />
    </div>
  );
};

export default Header;
