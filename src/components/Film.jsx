import React from "react";
import { CountRequest, RandomIdRequest } from "../apiRequest";
import PlanetInfo from "./PlanetInfo";
import NextPlanetButton from "./NextPlanetButton";

class Content extends React.Component {
  state = {
    isFetching: true,
    film: {},
    count: Number
  };

  componentWillMount() {
    CountRequest("films").then(response => {
      this.setState(
        {
          count: response
        },
        () => {
          this.filmRequest();
        }
      );
    });
  }

  filmRequest = () => {
    this.setState({
      isFetching: true
    });
    RandomIdRequest("films", this.state.count).then(response => {
      this.setState({
        film: response,
        isFetching: false
      });
    });
  };

  render() {
    return (
      <div className="content">
        <PlanetInfo
          title={this.state.film.title}
          firstItemKey={Object.keys(this.state.film)[3]}
          firstItemValue={this.state.film.episode_id}
          secondItemKey={Object.keys(this.state.film)[5]}
          secondItemValue={this.state.film.director}
          thirdItemKey={Object.keys(this.state.film)[6]}
          thirdItemValue={this.state.film.release_date}
          fourthItemKey={Object.keys(this.state.film)[4]}
          fourthItemValue={this.state.film.opening_crawl}
          fetching={this.state.isFetching}
        />
        <NextPlanetButton
          clickHandler={this.filmRequest}
          disabled={this.state.isFetching}
        />
      </div>
    );
  }
}

export default Content;
