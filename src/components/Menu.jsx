import React from "react";
import axios from "axios";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class Menu extends React.Component {
  state = {
    options: {},
    chooseOption: ""
  };

  componentWillMount() {
    axios.get("http://swapi1111.herokuapp.com/").then(response => {
      this.setState({
        options: response.data[0]
      });
    });
  }

  listClickHandler = option => {
    this.setState({
      chooseOption: option
    });
  };

  renderItems() {
    return Object.keys(this.state.options).map((item, index) => (
      <li key={index} onClick={() => this.listClickHandler(item)}>
        <Link to={`/${item}`}>{item.replace(/^\w/, c => c.toUpperCase())}</Link>
      </li>
    ));
  }

  render() {
    return (
      <div className="Menu">
        <div className="content">
          <ul>{this.renderItems()}</ul>
        </div>
      </div>
    );
  }
}

export default Menu;
