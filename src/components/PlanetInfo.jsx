import React from "react";
import loadingImage from "../assets/images/loading-image.png";

const LoadingComponent = (
  <div className="planet-info">
    <h1>Loading...</h1>
    <img className="loading-image" src={loadingImage} alt="loading" />
  </div>
);

const PlanetLoaded = props => (
  <div className="planet-info">
    <h1>{props.title}</h1>
    <ul>
      {
        <li>
          <h3>{props.firstItemKey}: </h3> {props.firstItemValue}
        </li>
      }
      {
        <li>
          <h3>{props.secondItemKey}: </h3> {props.secondItemValue}
        </li>
      }
      {
        <li>
          <h3>{props.thirdItemKey}: </h3> {props.thirdItemValue}
        </li>
      }
      {
        <li>
          <h3>{props.fourthItemKey}: </h3> {props.fourthItemValue}
        </li>
      }
    </ul>
  </div>
);

const PlanetInfo = function(props) {
  if (props.fetching) {
    return LoadingComponent;
  } else {
    return PlanetLoaded(props);
  }
};

export default PlanetInfo;
